import { POKEMONS } from "./mock-pokemon";

export class InMemoryDataService implements InMemoryDataService {
        createDb() {
                let pokemons = POKEMONS;
                return { pokemons };
        }
}

// GET api/pokemons
// GET api/pokemons/1
// PUT api/pokemons/1
// GET api/pokemons?name=^exp