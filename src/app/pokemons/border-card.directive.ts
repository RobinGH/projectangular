import { Directive, ElementRef, HostListener, Input } from "@angular/core";

@Directive({
    selector: '[pkmnBorderCard]'
})
export class BorderCardDirective {
    constructor(private el: ElementRef) {
        this.setHeight();
    }

    @Input("pkmnBorderCard") borderColor: string;

    // Ecoute mon entrée de souris
    // Modifie sa bordure avec une couleur
    @HostListener('mouseenter') onMouseEnter() {
        this.setBorder(this.borderColor || "#OO8000");
        //et → &&
        //ou → ||
    }

    // Ecoute ma sortie de souris
    // Reinitialiser la bordure
    @HostListener('mouseleave') onMouseLeave() {
        this.setBorder('#f5f5f5');
    }

    // Methodes
    // Modifier la bordure

    private setBorder(color: string) {
        // Créer une bordure verte
        let border = 'solid 4px ' + color;
        // Modifier mon élement courant avec cette bordure
        this.el.nativeElement.style.border = border;
    }

    private setHeight(){
        this.el.nativeElement.style.height = "180px";
    }
    
}