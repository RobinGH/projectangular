import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { POKEMONS } from '../mock-pokemon';
import { Pokemon } from '../pokemon';
import { PokemonService } from '../pokemon.service';

@Component({
  selector: 'app-list-pokemon',
  templateUrl: './list-pokemon.component.html',
  styleUrls: ['./list-pokemon.component.scss']
})

export class ListPokemonComponent implements OnInit {
  pokemons: Pokemon[];
  pokemonName: string;

  //string, number // Boolean, Date

  isSpecial: Boolean = false;
  targett: string;
  display: Boolean = false;

  constructor(private router: Router, private _pokemonService: PokemonService){

  }
  
  ngOnInit(): void{
    // this.pokemons = POKEMONS;
    // console.log(this.pokemons)
    // console.log(this.pokemonName)

    // this.pokemonName = "Salamèche";
    // this.pokemons = this._pokemonService.getPokemons();

    this._pokemonService.getPokemons().subscribe(data => {
      this.pokemons = data;
    })
  }
  
  clickBtn(){
    console.log("Clicked")
  }

  onKeyUp(event: any){
    this.targett = event.target.value;
  }

  selectPokemon(pokemon: Pokemon){
    // console.log("Clicked")
    this.router.navigate(["pokemon",pokemon.id])
  }
}
