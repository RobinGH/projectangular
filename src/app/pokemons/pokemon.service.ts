import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { POKEMONS } from "./mock-pokemon";
import { Pokemon } from "./pokemon";

@Injectable()
export class PokemonService{
    
    private pokemonUrl = "api/pokemons";

    constructor(private http: HttpClient){

    }

    getPokemons() : Observable<Pokemon[]> {
        //retourner la liste des pokémons depuis notre const 
        // return POKEMONS;
        return this.http.get<Pokemon[]>(this.pokemonUrl);
    }

    getPokemon(id: number) : Observable<Pokemon> {
        // let pokemons = this.getPokemons();
        // return pokemons.find(x => x.id == id);
        // const url = this.pokemonUrl + "/" + id;
        const url = `${this.pokemonUrl}/${id}`
        return this.http.get<Pokemon>(url);
    }

    getPokemonTypes(): Array<string> {
        return [
            'Plante', 'Feu', 'Eau', 'Insecte', 'Normal', 'Electrik',
            'Poison', 'Fée', 'Vol', 'Combat', 'Psy'
        ];
    }

}