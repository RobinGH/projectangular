import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DetailPokemonComponent } from "./detail-pokemon.component";
import { EditPokemonComponent } from "./edit-pokemon/edit-pokemon.component";
import { ListPokemonComponent } from "./list-pokemon/list-pokemon.component";

const appRoutes: Routes = [
    //routes
    {path: "pokemons", component: ListPokemonComponent},
    {path: "pokemon/edit/:id", component: EditPokemonComponent},
    {path: "pokemon/:id", component: DetailPokemonComponent},
];

@NgModule({
    imports: [
        RouterModule.forChild(appRoutes)   
    ],
    exports: [
        RouterModule
    ]
})
export class PokemonRoutingModule {

}