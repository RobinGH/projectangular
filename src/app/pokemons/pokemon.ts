export class Pokemon {
    name: string;
    hp: number;
    cp: number;
    picture: string;
    types: Array<string>;
    created: Date;
    id: number;
}