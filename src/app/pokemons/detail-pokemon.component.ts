import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { POKEMONS } from './mock-pokemon';
import { Pokemon } from './pokemon';
import { PokemonService } from './pokemon.service';

@Component({
  selector: 'detail-pokemon',
  templateUrl: './detail-pokemon.component.html',
  styleUrls: ['./detail-pokemon.component.scss']
})

export class DetailPokemonComponent implements OnInit {
    
    pokemons: Pokemon[];
    pokemon: Pokemon;
    
    constructor(
        private route: ActivatedRoute, 
        private router: Router,
        private _pokemonService: PokemonService) { }
  
    ngOnInit(): void{
        this.pokemons = POKEMONS;
        let id = Number(this.route.snapshot.paramMap.get("id"));

            // for (let index = 0; index < this.pokemons.length; index++){
            //     if (this.pokemons[index].id == id) {
            //         this.pokemon = this.pokemon[index];
            //     }
            // }

            //expression lambda ""=>""
            // this.pokemon = this.pokemons.find(x => x.id == id);

        // this.pokemon = this._pokemonService.getPokemon(id);
        this._pokemonService.getPokemon(id).subscribe(data=> {
            this.pokemon = data;
        })
    }         

    goBack(){
        this.router.navigate(['/pokemons']);
    }

    goEdit(pokemonId: number){
        this.router.navigate(['/pokemon/edit', pokemonId]);
    }
}



